// 2016-01-12 Version
var gulp = require('gulp');

var uglify = require('gulp-uglify');
// 2016년 01월 11일 새로이 추가. package에 설치만 해놓음
var cssnano = require('gulp-cssnano');
//var minifyCss = require('gulp-minify-css');
var autoprefixer = require('gulp-autoprefixer');
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant'); // npm i -D imagemin-pngquant
var sourcemaps = require('gulp-sourcemaps');
var csslint = require('gulp-csslint');
var jslint = require('gulp-jslint');
var sass = require('gulp-ruby-sass');
//var browserSync = require('browser-sync').create();
var browserSync = require('browser-sync');
var reload = browserSync.reload;
var jade = require('gulp-jade');


var dev_path = {
    default: 'dev/',
    img: 'dev/img/',
    scss: 'dev/scss/',
    css: 'dev/css/',
    js: 'dev/js/',
    jade: 'dev/jade/'
};
var build_path = {
    default: 'src/',
    img: 'src/img/',
    css: 'src/css/',
    js: 'src/js/'
};

/*
    Only Dev
 */

// Browser-Sync + Sass,Jade Compile
// terminal: gulp serve
gulp.task('serve',['sass','jade'],function(){
    browserSync.init({
        server: dev_path.default,
        open: true
    });
    gulp.watch(dev_path.scss+'*.scss',['sass']);
    gulp.watch(dev_path.jade+'*.jade',['jade']);
});

// Jade Compile
gulp.task('jade',function(){
    gulp.src(dev_path.jade+'*.jade')
        .pipe(jade({pretty: true}))
        .on('error',console.log)
        .pipe(gulp.dest(dev_path.default))
        .pipe(reload({stream: true}))
});

// SCSS Compile
gulp.task('sass',function(){
    return sass(dev_path.scss+'*.scss', { sourcemap: true, style: 'expanded'})
        .on('error', sass.logError)
        .pipe(sourcemaps.write())
        .pipe(sourcemaps.write('maps',{
            includeContent: false,
            sourceRoot: dev_path.css
        }))
        .pipe(gulp.dest(dev_path.css))
        .pipe(reload({stream: true}))
});

/*
    Only Build
 */
gulp.task('img-compress', function(){
   //return gulp.src('images/*')
    return gulp.src(dev_path.img+'*')
    .pipe(imagemin({
        progressive: true,
        svgoPlugins: [{removeViewBox: false}],
        use: [pngquant()]
    }))
    .pipe(gulp.dest(build_path.img));
});


gulp.task('js-compress', function() {
    //return gulp.src('js/*.js')
    return gulp.src(dev_path.js+'*.js')
        .pipe(uglify())
        .pipe(gulp.dest(build_path.js));
});

gulp.task('autoPrefixer',function(){
    //return gulp.src('css/*.css')
    return gulp.src(dev_path.css+'*.css')
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest(build_path.css));
});

gulp.task('minify-css',function(){
   //return gulp.src('css/*.css')
    return gulp.src(dev_path.css+'*.css')
       .pipe(minifyCss({compatibility: 'ie8'}))
       .pipe(gulp.dest(build_path.css));
});

gulp.task('copy:html',function(){
   return gulp.src(dev_path.default+'*.html')
       .pipe(gulp.dest(build_path.default));
});

//gulp.task('compress',['img-compress', 'js-compress', 'autoPrefixer','minify-css'],function(){
gulp.task('compress',['img-compress', 'js-compress', 'autoPrefixer','copy:html'],function(){
    console.log('Complete');
});



// Lint Check
gulp.task('css-lint', function(){
    gulp.src('css/*.css')
    .pipe(csslint())
    .pipe(csslint.reporter())
});

gulp.task('js-lint',function(){
    gulp.src('js/*.js')
    .pipe(jslint({
        node: true,
        evil: true,
        nomen: true,
        global: [],
        predef: [],
        reporter: 'default',
        edition: '',
        errorsOnly: false
    })).on('error',function(error){
        console.error(String(error));
    });
});


gulp.task('lint',['css-lint, js-lint'],function(){
    console.log('Validation Check Complete');
});
//gulp.task('default', ['serve','sass','img-compress','js-compress','hello','minify-css', 'autoPrefixer']);
