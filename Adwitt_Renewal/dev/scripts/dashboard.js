// Select2 Scriptxx
var select2Maker = function(thisElement){
    var _this = thisElement;

    if (_this.data('search')) {
        _this.select2({
            theme : "adwitt"
        });
    }
    else if (_this.data('time')){
        _this.select2({
            minimumResultsForSearch: Infinity,
            theme : "adwitt-time"
        });
    }
    else {
        _this.select2({
            minimumResultsForSearch: Infinity,
            theme : "adwitt"
        });
    }

    _this.on('select2:opening', function() {
        $('.select2-results__options').perfectScrollbar().perfectScrollbar('update');
    });
    _this.on('select2:closing', function() {
        $('.select2-results__options').perfectScrollbar('disabled');
    });
};

// Drag로 Sort 하는 부분

var _list = Sortable.create(document.getElementById('list'), {
    handle: ".drag-handle",
    sort: -1 // 기존에서 번호가 -1씩 정렬
});

// Select2

$('select').each(function(){
   select2Maker($(this));
});

$('#campaign-type__wrapper').on('click','button',function(){
    var _this = $(this);
    if (_this.hasClass('campaign-type__button--active')){
        _this.removeClass('campaign-type__button--active');
        _this.find('img').attr('src',function(){
            return this.src.replace('ffffff','404040')
        });
    }
    else {
        _this.addClass('campaign-type__button--active');
        _this.find('img').attr('src',function(){
            return this.src.replace('404040','ffffff')
        });
    }
});