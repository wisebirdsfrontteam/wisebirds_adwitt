# Adwitt Renewal Project

## Purpose

### Basic Rule

Reset CSS는 [normalize.css](github.com/necolas/normalize.css) 를 사용한다.
2016년 01월 12일자 최신버전은 v3.0.3
 
 
### Layout

#wrap
|---#header : Include Logo, GNB, Setting
|---#container
    |---.row
        |---.col
            |---.content : * padding:0 20px 
|---#footer : Include Address

### Class or ID Naming Rule

### File Naming Rule

Case :
    - Image
    - Javascript
    - Stylesheet
    

### Code Style

.content {
    padding-left: 37px;
    padding-right: 37px;
}

### Caution

select 태그는 select2 라이브러리를 이용하는데 이로 인해 인라인 스타일로 크기를 지정해주는 것이 좋다.